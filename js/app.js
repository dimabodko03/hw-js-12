const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
    const key = event.key;
    buttons.forEach(button => {
        if (button.getAttribute('data-key') === key) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    });
});